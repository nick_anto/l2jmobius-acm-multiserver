
## Screenshots

![Login Page](https://i.postimg.cc/hvpX8G5n/login.jpg)

![Settings](https://i.postimg.cc/JzgBdG44/settings.jpg)

![Change password](https://i.postimg.cc/jqQPdxvZ/change-password.jpg)

![Donation page](https://i.postimg.cc/hPDxXk5g/donate.jpg)

![Edit account](https://i.postimg.cc/52jC3wJR/edit-account.jpg)

![Edit character](https://i.postimg.cc/RVgtKgkt/edit-character.jpg)