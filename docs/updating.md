
## Updating L2jMobius ACM

If a new version of L2jMobius ACM is pushed to this repository, follow these steps:

### Navigate to Your Project Directory

Change your current directory to the project's directory using the `cd` command. 

```bash
cd [path/to/your/project]
```

### Pull the Latest Changes

Run the following `git pull` command to fetch and merge the latest changes from the remote repository:

```bash
git pull origin
```

### Verify the Update

After running git pull, you should see messages indicating the progress and completion of the update process. Ensure there are no error messages.

